import SwiftUI

@main
struct SoundriveApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
